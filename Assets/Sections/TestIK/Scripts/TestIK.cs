﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestIK : MonoBehaviour
{
    protected Animator _animator;
    public bool ikActive = false;
    public Transform TargetTransform = null;
    public float Weight;

	// Use this for initialization
	void Start ()
	{
	    _animator = this.GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnAnimatorIK()
    {
        if (_animator)
        {
            if (ikActive)
            {
                setWeight(AvatarIKGoal.RightHand, Weight);
                setWeight(AvatarIKGoal.RightHand,Weight);
                setWeight(AvatarIKGoal.LeftHand, Weight);
                setWeight(AvatarIKGoal.RightFoot,Weight);
                setWeight(AvatarIKGoal.LeftFoot, Weight);
                _animator.SetLookAtWeight(Weight);
                if (TargetTransform != null)
                {
                    setValue(AvatarIKGoal.RightHand, TargetTransform);
                    setValue(AvatarIKGoal.LeftHand, TargetTransform);
                    setValue(AvatarIKGoal.RightFoot, TargetTransform);
                    setValue(AvatarIKGoal.LeftFoot, TargetTransform);
                    _animator.SetLookAtPosition(TargetTransform.position);
                }
            }
            else
            {
                setWeight(AvatarIKGoal.RightHand, 0);
                setWeight(AvatarIKGoal.RightHand,0);
                setWeight(AvatarIKGoal.LeftHand, 0);
                setWeight(AvatarIKGoal.RightFoot,0);
                setWeight(AvatarIKGoal.LeftFoot, 0);
                _animator.SetLookAtWeight(0);
            }
        }
    }

    void setWeight(AvatarIKGoal ikSection, float weight)
    {
        _animator.SetIKPositionWeight(ikSection, Weight);
        _animator.SetIKRotationWeight(ikSection, Weight);
    }

    void setValue(AvatarIKGoal ikSection, Transform target)
    {
        _animator.SetIKPosition(ikSection, target.position);
        _animator.SetIKRotation(ikSection, target.rotation);
    }
}
