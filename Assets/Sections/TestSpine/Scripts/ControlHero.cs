﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Permissions;
using Spine;
using Spine.Unity;
using UnityEngine;
using UnityEngine.UI;
using Animation = Spine.Animation;
using Event = Spine.Event;

public class ControlHero : MonoBehaviour
{
    public SkeletonAnimation skeletonAnimation;
    private Spine.AnimationState spineaAnimationState;
    private Skeleton skeleton;
    private List<String> animationNames = new List<string>() {"attack", "run"};

    private List<TrackEntry> tes = new List<TrackEntry>();

    // Use this for initialization
    void Awake()
    {
    }

    // Update is called once per frame
    //private bool judge = true;
    void Update()
    {
        //if (judge)
        //{
        //    spineaAnimationState.Event += HandleEvent;
        //    judge = false;
        //}

        //skeleton.flipX = !skeleton.flipX;
        //skeletonAnimation.timeScale = TimeScaleSlider.value;

        //if (Input.GetKeyDown(KeyCode.A))
        //{
        //    //tes[0].timeScale = 0f;
        //    skeleton.PoseWithAnimation("run", 1, true);
        //}
        //if (Input.GetKeyDown(KeyCode.D))
        //{
        //    //tes[0].timeScale = 0f;
        //    skeleton.PoseWithAnimation("attack", 1, true);
        //}
    }

    void HandleEvent(TrackEntry te, Spine.Event e)
    {
        //Debug.Log("time:"+te.AnimationTime);
        //if (te.AnimationTime <= 0.01f)
        //{
        //    Debug.Log(1);
        //}
        //if (te.AnimationTime <= 0.6f && te.AnimationTime >= 0.4f)
        //{
        //    Debug.Log(4);
        //}

        //if (te.IsComplete)
        //{
        //    AudioSource.Play();
        //    spineaAnimationState.Start += delegate(TrackEntry entry)
        //    {
        //        spineaAnimationState.Event += HandleEvent;
        //    };
        //}
    }
    void Start()
    {
        skeleton = skeletonAnimation.Skeleton;
        spineaAnimationState = skeletonAnimation.state;
        //skeletonAnimation.timeScale = 0.1f;

        tes.Add(spineaAnimationState.SetAnimation(0, "walk", true));

        Del del = DelC.DelMod;
        Debug.Log("del:"+ del("ios"));
    }

    public delegate string Del(string s1);

    public class DelC
    {
        public static string DelMod(string s1)
        {
            Debug.Log("DelMod:" + s1);
            return s1;
        }
    }
}
