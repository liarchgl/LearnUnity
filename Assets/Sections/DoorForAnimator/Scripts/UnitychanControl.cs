﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitychanControl : MonoBehaviour
{
    private Camera _mainCamera;
    private Animator _animator;
	// Use this for initialization
	void Start () {
		_mainCamera = Camera.main;
	    _animator = this.GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update ()
	{
	    float controlV = Input.GetAxis("Vertical");
	    float controlH = Input.GetAxis("Horizontal");
	    Vector3 dirLocal = new Vector3(controlH, 0, controlV).normalized;
	    Vector3 dirWorld = Quaternion.Euler(0, _mainCamera.transform.rotation.eulerAngles.y, 0) * dirLocal;
        _animator.SetBool("Run", dirWorld.magnitude > 0.2f);
        this.transform.LookAt(this.transform.position + dirWorld);
	}
}
