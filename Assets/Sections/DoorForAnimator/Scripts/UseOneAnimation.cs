﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UseOneAnimation : MonoBehaviour
{
    private Animation _animation;
	// Use this for initialization
	void Start ()
	{
	    _animation = this.transform.GetChild(0).GetComponent<Animation>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerExit(Collider other)
    {
        AnimationState state = _animation["BollTransfer"];
        state.time = state.length;
        state.speed = -3;
        _animation.Play("BollTransfer");
    }
    private void OnTriggerEnter(Collider other)
    {
        AnimationState state = _animation["BollTransfer"];
        state.time = 0;
        state.speed = 1;
        _animation.Play("BollTransfer");
    }
}
