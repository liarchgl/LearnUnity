﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorControl : MonoBehaviour
{
    private Animation _doorAnimation;
	// Use this for initialization
	void Start ()
	{
	    _doorAnimation = this.transform.GetChild(0).GetComponent<Animation>();
	}
	
	// Update is called once per frame
	void Update () {

	}
	/// <summary>
	/// OnTriggerEnter is called when the Collider other enters the trigger.
	/// </summary>
	/// <param name="other">The other Collider involved in this collision.</param>
	void OnTriggerEnter(Collider other)
	{
		_doorAnimation.Play("DoorOpen");
	}
	/// <summary>
	/// OnTriggerExit is called when the Collider other has stopped touching the trigger.
	/// </summary>
	/// <param name="other">The other Collider involved in this collision.</param>
	void OnTriggerExit(Collider other)
	{
		_doorAnimation.Play("DoorClose");
	}
}
