﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using XLua;

public class TestXLua : MonoBehaviour
{
    private LuaEnv luaenv;

    // Use this for initialization
    void Start ()
    {
        luaenv = new LuaEnv();
//        luaenv.AddLoader(delegate(ref string filepath)
//        {
//            string code = Resources.Load(filepath).ToString();
//            return System.Text.Encoding.UTF8.GetBytes(code);
//        });
//        luaenv.DoString("require('LuaCode.lua')");

        luaenv.DoString("a = {}");
        Dictionary<string, string> a = luaenv.Global.Get<Dictionary<string,string>>("a");
        Debug.Log(a);
    }

    // Update is called once per frame
    void Update ()
    {
        if (luaenv != null)
        {
            luaenv.Tick();
        }
    }

    private void OnDestroy()
    {
        luaenv.Dispose();
    }
}
