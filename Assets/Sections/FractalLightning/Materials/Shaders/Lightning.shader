﻿Shader "LeeSue/Lightning"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_TransparentTex ("Transparent Texture", 2D) = "white" {}
		_ShowSection ("how much to show", float) = 0
	}
	SubShader
	{
		Tags {"Queue"="AlphaTest"}
		LOD 100
		Lighting Off

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv0 : TEXCOORD0;
				float2 uv1 : TEXCOORD1;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			sampler2D _TransparentTex;
			float4 _TransparentTex_ST;
			float _ShowSection;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv0 = TRANSFORM_TEX(v.uv, _MainTex);
				o.uv1 = TRANSFORM_TEX(v.uv, _TransparentTex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				fixed4 col = tex2D(_MainTex, i.uv0);
				float a = tex2D(_TransparentTex, i.uv1).r;
				clip( _ShowSection - a);
				// apply fog
				return col;
			}
			ENDCG
		}
	}
}
