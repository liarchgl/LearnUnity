﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightningChange : MonoBehaviour
{
    private float _startTime;
    public float StayTime;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
	{
	    float showSection = (Time.time - _startTime) / StayTime;
        this.GetComponent<MeshRenderer>().material.SetFloat("_ShowSection",showSection);
	    if (Time.time - _startTime > StayTime)
	    {
        Debug.Log("time:"+Time.time);
        Debug.Log("start time:"+_startTime);
        Debug.Log("section:"+showSection);
            Destroy(this.gameObject);
	    }
    }

    void Awake()
    {
        _startTime = Time.time;
    }
}
