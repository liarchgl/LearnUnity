﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class TestProfiler : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	    for (int j = 0; j < 10; ++j)
        {
            ProfilerSample.BeginSample("Check");
            int x = 1;
            for (int i = 0; i < 1000000; ++i)
            {
                cel(ref x, i);
            }
            Thread t0 = new Thread(new ThreadStart(celMul));
            t0.IsBackground = true;
            t0.Start();
            ProfilerSample.EndSample();
            Debug.Log(x);
        }
    }

    void celMul()
    {
        int x = 0;
        for (int i = 0; i < 100000; ++i)
        {
            x += i;
        }
        Debug.Log(x);
    }
    void cel(ref int ans, int cheng)
    {
        ans *= cheng;
    }
}
