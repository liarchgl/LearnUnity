﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightningSegment
{
    public LightningSegment FrontChild, BackChild, Branch;
    public Vector3 StartPosition, EndPosition;
    public float uv;
    public int Deepth;

    public bool IsLeaf()
    {
        return FrontChild == null && BackChild == null;
    }

    public Vector3 Direction()
    {
        return StartPosition - EndPosition;
    }

    public Vector3 MiddlePosition()
    {
        return (StartPosition + EndPosition) / 2;
    }

    public LightningSegment(Vector3 start, Vector3 end)
    {
        StartPosition = start;
        EndPosition = end;
        uv = 0.5f;
        Deepth = 1;
        FrontChild = null;
        BackChild = null;
        Branch = null;
    }
    public static float[] arrayForUV = new float[]{
        1f, 0.5f, 0.25f, 0.125f, 0.0625f, 0.03125f,
        0.015625f, 0.0078125f, 0.00390625f, 0.001953125f,
        0.0009765625f, 0.00048828125f
    };
}

public class Lightning : MonoBehaviour
{
    public Transform HandTransform;
    public Transform TargeTransform;
    public int MaxDeepth;
    public float RateLightning;
    public float RateBranch;
    public float BaseAttenuation;
    public float RandomRangeX;
    public float RandomRangeY;
    public float LightningRadius;
    public GameObject PreLightning;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
	{
        if (Random.Range(0.0f, 1.0f) < RateLightning)
	    {
            CreateLightning(HandTransform.position, TargeTransform.position);
        }
    }

    private void CreateLightning(Vector3 startV3, Vector3 endV3)
    {
        LightningSegment ls = new LightningSegment(HandTransform.position, TargeTransform.position);
        Vector3 lightningDirection = ls.Direction();
        Vector3 forwardDirection, rightDirection;
        
        // 得出一个垂直于闪电方向的方向
        if (lightningDirection.x != 0)
        {
            forwardDirection = new Vector3(-(lightningDirection.y + lightningDirection.z), 1, 1);
        }
        else if (lightningDirection.y != 0)
        {
            forwardDirection = new Vector3(1, -(lightningDirection.x + lightningDirection.z)/lightningDirection.y, 1);
        }
        else if (lightningDirection.z != 0)
        {
            forwardDirection = new Vector3(1,1, -(lightningDirection.x+lightningDirection.y)/lightningDirection.z);
        }
        else
        {
            return;
        }
        // 叉乘得右向
        rightDirection = Vector3.Cross(lightningDirection, forwardDirection);

        // 单位化
        lightningDirection.Normalize();
        forwardDirection.Normalize();
        rightDirection.Normalize();

        GetFractcalLightning(ls, forwardDirection, rightDirection, lightningDirection);
        Mesh mesh = CreateMesh(ls);
        GameObject lightningGameObject = Instantiate(PreLightning).gameObject;
        lightningGameObject.GetComponent<MeshFilter>().mesh = mesh;
        lightningGameObject.transform.position = Vector3.zero;
        lightningGameObject.transform.parent = this.transform;
        lightningGameObject.SetActive(true);
    }

    private void GetFractcalLightning(LightningSegment ls, Vector3 forwardDirection, Vector3 rightDirection, Vector3 lightningDirection)
    {
        int deepth = ls.Deepth;
        if (deepth > MaxDeepth)
        {
            return;
        }
        Vector3 mid = ls.MiddlePosition();
        float x = Mathf.Exp(-BaseAttenuation * deepth) * Random.Range(RandomRangeX, RandomRangeY);
        float y = Mathf.Exp(-BaseAttenuation * deepth) * Random.Range(RandomRangeX, RandomRangeY);
        mid += x * forwardDirection;
        mid += y * rightDirection;
        ls.FrontChild = new LightningSegment(ls.StartPosition, mid)
        {
            Deepth = deepth + 1
        };
        ls.FrontChild.uv = ls.uv + LightningSegment.arrayForUV[ls.FrontChild.Deepth];
        ls.BackChild = new LightningSegment(mid, ls.EndPosition)
        {
            Deepth = deepth + 1
        };
        ls.BackChild.uv = ls.uv - LightningSegment.arrayForUV[ls.BackChild.Deepth];
        GetFractcalLightning(ls.FrontChild, forwardDirection, rightDirection, lightningDirection);
        GetFractcalLightning(ls.BackChild, forwardDirection, rightDirection, lightningDirection);
        if (Random.Range(0, 1) < RateBranch)
        {
            x = Mathf.Exp(-BaseAttenuation * deepth) * Random.Range(RandomRangeX, RandomRangeY);
            y = Mathf.Exp(-BaseAttenuation * deepth) * Random.Range(RandomRangeX, RandomRangeY);
            float z = Mathf.Exp(-BaseAttenuation * deepth) * Random.Range(RandomRangeX, RandomRangeY);
            Vector3 branchEndV3 = ls.MiddlePosition() + x * forwardDirection + y * rightDirection + z * lightningDirection;
            ls.Branch = new LightningSegment(mid, branchEndV3)
            {
                Deepth = deepth + 1,
                uv = ls.uv - LightningSegment.arrayForUV[deepth + 1]
            };
            GetFractcalLightning(ls.Branch, forwardDirection, rightDirection, lightningDirection);
        }
    }

    private Mesh CreateMesh(LightningSegment ls)
    {
        Vector3 cameraPosition = Camera.main.transform.position;
        List<Vector3> vertices = new List<Vector3>();
        List<int> triangles = new List<int>();
        List<Vector2> uvs = new List<Vector2>();
        LightningSegment2Mesh(ls, cameraPosition, vertices, triangles, uvs);

        Mesh mesh = new Mesh();
        mesh.SetVertices(vertices);
        mesh.triangles = triangles.ToArray();
        mesh.SetUVs(0, uvs);
        mesh.RecalculateNormals();
        mesh.RecalculateBounds();
        mesh.RecalculateTangents();
        return mesh;
    }

    private void LightningSegment2Mesh(LightningSegment ls, Vector3 cameraPosition, List<Vector3>vertices, List<int>triangles, List<Vector2>uvs)
    {
        if (ls.IsLeaf())
        {
            Vector3 mid = ls.MiddlePosition();
            Vector3 toCamera = cameraPosition - mid ;
            Vector3 lightningDirection = ls.EndPosition - ls.StartPosition;
            Vector3 vertical = Vector3.Cross(toCamera, lightningDirection);

            vertices.Add(ls.StartPosition + vertical * LightningRadius / 2);
            vertices.Add(ls.StartPosition - vertical * LightningRadius / 2);
            vertices.Add(ls.EndPosition + vertical * LightningRadius / 2);
            vertices.Add(ls.EndPosition - vertical * LightningRadius / 2);

            int idx = vertices.Count - 1;
            triangles.Add(idx);
            triangles.Add(idx - 1);
            triangles.Add(idx - 2);
            triangles.Add(idx - 1);
            triangles.Add(idx - 3);
            triangles.Add(idx - 2);

            uvs.Add(new Vector2(1, ls.uv));
            uvs.Add(new Vector2(0, ls.uv));
            uvs.Add(new Vector2(1, ls.uv));
            uvs.Add(new Vector2(0, ls.uv));
        }
        else
        {
            LightningSegment2Mesh(ls.FrontChild, cameraPosition, vertices, triangles, uvs);
            LightningSegment2Mesh(ls.BackChild, cameraPosition, vertices, triangles, uvs);
            if (ls.Branch != null)
            {
                LightningSegment2Mesh(ls.Branch, cameraPosition, vertices, triangles, uvs);
            }
        }
    }
}
