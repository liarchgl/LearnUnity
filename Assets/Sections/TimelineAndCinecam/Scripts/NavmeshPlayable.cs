﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Playables;

public class NavmeshPlayable : PlayableBehaviour
{
    public float AgentSpeed;
    public PlayableDirector Director;
    public NavMeshAgent Agent;
    public Transform Target;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public override void OnGraphStart(Playable playable)
    {
        base.OnGraphStart(playable);
    }

    public override void ProcessFrame(Playable playable, FrameData info, object playerData)
    {
        base.ProcessFrame(playable, info, playerData);
        if (Application.isPlaying)
        {
            if (Director != null)
            {
                Agent.SetDestination(Target.position);
                Agent.speed = AgentSpeed;
            }
        }
    }
}
